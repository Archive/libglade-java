/*
 *  This file is part of Java-GTK Mandel -
 *  Copyright (c) 2001 [Author]
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 *Description of the Class
 *
 * @author     mbusse
 * @created    12. November 2001
 */

package mandel;

public class JavaGTKMandelMath extends java.lang.Object {

    /**
     * Constructor for the MandelMath object
     */
    public JavaGTKMandelMath() {
    }

    /**
     * returns the real part of x
     * 
     * @param width
     *            width of the window
     * @param x
     *            position of x in the window
     * @param reMin
     *            minimal real value
     * @param reMax
     *            maximal real value
     * @return the real part corresponding to x
     */
    public double realPart(int width, int x, double reMin, double reMax) {
        return ((reMax - reMin) * x / width + reMin);
    }

    /**
     * returns the imaginary part of y
     * 
     * @param height
     *            height of the window
     * @param y
     *            position of y in the window
     * @param imMin
     *            minimal imaginary value
     * @param imMax
     *            maximal imaginary value
     * @return the imaginary part corresponding to y
     */
    public double imPart(int height, int y, double imMin, double imMax) {
        return ((imMax - imMin) * y / height + imMin);
    }

    /**
     * computes a given point
     * 
     * @param depth
     *            number of iterations
     * @param part_re
     *            real part of the actual computed value
     * @param part_im
     *            imaginary part of the actual computed value
     * @param rest_re
     * @param rest_im
     * @return 0 or the depth when the point was "out of bounds"
     */
    public short compute(int depth, double part_re, double part_im,
            double rest_re, double rest_im) {
        double re_new = part_re;
        double im_new = part_im;
        double re2 = 0;
        short d1 = 0;
        while (!((d1 == depth) || ((re_new * re_new) + (im_new * im_new) > 100))) {
            ++d1;
            re2 = re_new * re_new - im_new * im_new + rest_re;
            im_new = 2 * re_new * im_new + rest_im;
            re_new = re2;
        }
        if (d1 == depth) {
            return (0);
        } else {
            return (d1);
        }

    }
}
