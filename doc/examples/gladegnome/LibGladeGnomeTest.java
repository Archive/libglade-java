package gladegnome;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.gnu.glade.GladeXMLException;
import org.gnu.glade.LibGlade;
import org.gnu.gnome.Program;
import org.gnu.gtk.Gtk;

class LibGladeGnomeTest {

    public LibGladeGnomeTest(String filename) {
        try {
            LibGlade glade = new LibGlade(filename, this, null);
        } catch (GladeXMLException e) {
            System.err.println("Error parsing glade XML file." + e);
        } catch (FileNotFoundException e) {
            System.err.println("Glade XML file not found.");
        } catch (IOException e) {
            System.err.println("Error reading glade XML file.");
        }

    }

    //
    // Callback definitions
    //

    // These callbacks cater for all the examples so some callback methods may
    // be redundant for the glade file that you are investigating.

    public void notImplemented() {
        System.out.println("notImplemented");
    }

    public void gtkMainQuit() {
        Gtk.mainQuit();
    }

    public void notImplemented(int i) {
        System.out.println("notImplemented");
    }

    public void gtkMainQuit(int i) {
        Gtk.mainQuit();
    }

    //
    // The main program
    //
    public static void main(String args[]) {
        if (args.length != 1) {
            System.out.println("Usage\n\tLibGladeGnomeTest <glade-filename>\n");
            System.exit(0);
        }

        // First call Gnome.init to initialize everything
        Program prog = Program.initGnome("GladeTest", "0.1", args);

        // Then initialize the constructor which will put everything together
        LibGladeGnomeTest test = new LibGladeGnomeTest(args[0]);

        // Start the main Gtk loop
        Gtk.main();
    }
}
