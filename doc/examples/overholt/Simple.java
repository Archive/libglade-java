package overholt;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.gnu.glade.GladeXMLException;
import org.gnu.glade.LibGlade;
import org.gnu.gtk.Entry;
import org.gnu.gtk.Gtk;

public class Simple
{

	private LibGlade	glade;

	Entry				e;

	public Simple() throws GladeXMLException, FileNotFoundException, IOException {
		glade = new LibGlade("overholt/simple.glade", this);
		e = (Entry) glade.getWidget("entry1");
	}

	public void on_button1_clicked() {
		System.out.println(e.getText());
	}

	public void gtkMainQuit() {
		Gtk.mainQuit();
	}

	public static void main(String[] args) {

		Simple simple;

		try {
			Gtk.init(args);
			simple = new Simple();
			Gtk.main();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
