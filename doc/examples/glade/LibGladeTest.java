package glade;

import java.io.IOException;

import org.gnu.glade.LibGlade;
import org.gnu.gtk.ColorSelectionDialog;
import org.gnu.gtk.Dialog;
import org.gnu.gtk.FileSelection;
import org.gnu.gtk.FontSelectionDialog;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.InputDialog;

public class LibGladeTest
{
	Dialog					dialog;

	FileSelection			fileSelection;

	ColorSelectionDialog	colorSelectionDialog;

	FontSelectionDialog		fontSelectionDialog;

	InputDialog				inputDialog;

	LibGlade				glade;

	public LibGladeTest(String filename) {
		try {
			glade = new LibGlade(filename, this);
		} catch (IOException e) {
			System.out.println("failed: " + e);
			System.exit(1);
		}
	}

	//
	// Callback definitions
	//

	// These callbacks cater for all the examples so some callback methods may
	// be redundant for the glade file that you are investigating.

	public void gtkMainQuit() {
		Gtk.mainQuit();
	}

	public void gtk_main_quit() {
		Gtk.mainQuit();
	}

	public void gtkWidgetDestroy() {
		Gtk.mainQuit();
	}

	public void notImplemented() {
		System.out.println("Not implemented");
	}

	// Dialog specific callback
	public void gtkDialogHide() {
		dialog = (Dialog) glade.getWidget("dialog1");
		if (dialog != null) {
			dialog.hide();
		}
	}

	// FileSelection specific callback
	public void gtkFileSelHide() {
		fileSelection = (FileSelection) glade.getWidget("fileselection1");
		if (fileSelection != null) {
			fileSelection.hide();
		}
	}

	// ColorSelectionDialog specific callback
	public void gtkColorSelDialogHide() {
		colorSelectionDialog = (ColorSelectionDialog) glade.getWidget("colorselectiondialog1");
		if (colorSelectionDialog != null) {
			colorSelectionDialog.hide();
		}
	}

	// ColorSelectionDialog specific callback
	public void gtkFontSelDialogHide() {
		fontSelectionDialog = (FontSelectionDialog) glade.getWidget("fontselectiondialog1");
		if (fontSelectionDialog != null) {
			fontSelectionDialog.hide();
		}
	}

	// ColorSelectionDialog specific callback
	public void gtkInputDialogHide() {
		if (inputDialog != null) {
			inputDialog.hide();
		}
	}

	//
	// The main program
	//
	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("Usage\n\tLibGladeTest <glade-filename>\n");
			System.exit(0);
		}

		// First call GTk.init to initialize everything
		Gtk.init(args);

		// Then initialize the constructor which will put everything together
		new LibGladeTest(args[0]);

		// Start the main Gtk loop
		Gtk.main();
	}
}
