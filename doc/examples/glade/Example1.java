package glade;

import java.io.IOException;

import org.gnu.glade.LibGlade;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.LifeCycleEvent;

public class Example1
{

	private static final String	gladeFilename	= "glade/example1.glade";

	public Example1() throws IOException {
		new LibGlade(gladeFilename, this);
	}

	// Referenced in example1.glade
	public void on_button1_clicked(ButtonEvent event, Object target) {
		System.out.println("on_button1_clicked invoked, exiting...");
		Gtk.mainQuit();
	}

	public void on_window1_delete_event(LifeCycleEvent event, Object target) {
		System.out.println("on_window1_delete invoked, exiting...");
		Gtk.mainQuit();
	}

	public static void main(String args[]) {
		Gtk.init(args);
		try {
			new Example1();
		} catch (IOException e) {
			System.err.println(e.toString());
			System.exit(1);
		}

		// Start the main Gtk loop
		Gtk.main();
	}
}
