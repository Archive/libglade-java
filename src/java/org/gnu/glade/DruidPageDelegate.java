package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gnome.event.DruidPageChangeEvent;
import org.gnu.gnome.event.DruidPageChangeListener;
import org.gnu.gnome.event.DruidPageSetupEvent;
import org.gnu.gnome.event.DruidPageSetupListener;

/**
 * DruidPageListener delegate class.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
class DruidPageDelegate extends ListenerDelegate implements
        DruidPageChangeListener, DruidPageSetupListener {

    public DruidPageDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public boolean druidPageChangeEvent(DruidPageChangeEvent event) {
        return fireEvent(event);
    }

    public void druidPageSetupEvent(DruidPageSetupEvent event) {
        fireEvent(event);
    }

}
