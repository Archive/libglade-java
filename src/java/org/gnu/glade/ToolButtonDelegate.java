/* 
 * LibGlade support for libglade for Java-Gnome
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gtk.event.ToolButtonEvent;
import org.gnu.gtk.event.ToolButtonListener;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
public class ToolButtonDelegate extends ListenerDelegate implements
        ToolButtonListener {

    public ToolButtonDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public boolean toolButtonEvent(ToolButtonEvent event) {
        fireEvent(event);
        return false;
    }
}
