package org.gnu.glade;

/**
 * Exception thrown due to XML parsing errors of a glade file.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
public class GladeXMLException extends java.io.IOException {

	 /**
	  * @deprecated
	  */
    public GladeXMLException(String s) {
        super(s);
    }

}
