package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gtk.event.MouseEvent;
import org.gnu.gtk.event.MouseListener;

/**
 * MouseListener delegate class.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
class MouseDelegate extends ListenerDelegate implements MouseListener {

    public MouseDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public boolean mouseEvent(MouseEvent event) {
        return fireEvent(event);
    }
}
