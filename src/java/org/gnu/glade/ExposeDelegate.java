/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gtk.event.ExposeEvent;
import org.gnu.gtk.event.ExposeListener;

/**
 * ExposeListener delegate class.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
public class ExposeDelegate extends ListenerDelegate implements ExposeListener {

    public ExposeDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public boolean exposeEvent(ExposeEvent event) {
        return fireEvent(event);
    }
}
