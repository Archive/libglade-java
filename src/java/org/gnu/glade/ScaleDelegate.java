package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gtk.event.ScaleEvent;
import org.gnu.gtk.event.ScaleListener;

/**
 * ScaleListener delegate class.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
class ScaleDelegate extends ListenerDelegate implements ScaleListener {

    public ScaleDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public String formatScaleValue(ScaleEvent event, double value) {
        try {
            Object[] params = new Object[] { event, new Double(value) };
            return (String) handler.invoke(owner, params);
        } catch (Exception e) {
            System.err.println("failed calling " + owner.getClass().getName()
                    + '.' + handler.getName() + ": " + e);
        }
        return "";
    }
}
