/* 
 * LibGlade support for libglade for Java-Gnome
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 * 
 * Author: Jean van Wyk
 * Copyright 2000 Jean van Wyk, all rights reserved. 
 * 
 * Special Thanks to Avi Bryant for writing the original version (GladeXML)
 * for GTK.
 */

package org.gnu.glade;

import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Hashtable;

import org.gnu.gtk.Widget;
import org.gnu.glib.Handle;

/**
 * libglade support for Java-GNOME
 * 
 * @author Avi Bryant
 * @author Jean van Wyk
 * @author Julian Fitzell
 * @author Tom Ball
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. See <code>org.gnome.glade.LibGlade</code> for the
 *             new version in java-gnome 4.0.
 */
public class LibGlade {
    private Handle gladeHandle;

    protected Hashtable widgets = new Hashtable();

    protected Object owner;

    /**
     * Create a new LibGlade object from a Glade XML definition file. The
     * definition file is read, parsed, and a GTK widget set is mapped. Each
     * LibGlade object is "owned" by an object that provides event handling for
     * all the defined widgets.
     * 
     * This class supports the <a
     * href="http://glade.gnome.org/glade-2.0.dtd">Glade 2.0 XML schema</a>.
     * Glade 1.0 files (those that start with the
     * <code>&lt;GTK-Interface&gt;</code> tag) can be converted using
     * libglade-convert (part of the <code>libglade2-devel</code> package).
     * 
     * @param file
     *            the path of the Glade 2.0 XML definition file.
     * @param owner
     *            the default object for event handling
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */
    public LibGlade(String file, Object owner) throws GladeXMLException,
            FileNotFoundException, IOException {
        this(file, owner, null);
        this.owner = owner;
    }

    /**
     * Create a new LibGlade object from a Glade XML definition file. The
     * definition file is read, parsed, and a GTK widget set is mapped. Each
     * LibGlade object is "owned" by an object that provides event handling for
     * all the defined widgets.
     * 
     * This class supports the <a
     * href="http://glade.gnome.org/glade-2.0.dtd">Glade 2.0 XML schema</a>.
     * Glade 1.0 files (those that start with the
     * <code>&lt;GTK-Interface&gt;</code> tag) can be converted using
     * libglade-convert (part of the <code>libglade2-devel</code> package).
     * 
     * An optional widget node root may be specified to build a fragment of a
     * widget tree (specify <code>null</code>. From the LibGlade 2.0
     * documentation: "This feature is useful if you only want to build say a
     * toolbar or menu from the XML file, but not the window it is embedded in."
     * 
     * @param file
     *            the path of the Glade 2.0 XML definition file.
     * @param owner
     *            the default object for event handling
     * @param root
     *            the root widget node for a sub-tree, or <code>null</code>
     *            for the complete tree.
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */
    public LibGlade(String file, Object owner, String root)
            throws GladeXMLException, FileNotFoundException, IOException {
        // When you call glade_xml_new_from_buffer, it looses
        // information about what the glade file was, and later runs
        // into issues if you try to instantiate anything that needs a
        // path relative to that file for example, a GtkPibuf.

        if (new File(file).canRead() == false) {
            throw new FileNotFoundException("File does not exist.");
        }
        this.owner = owner;
        gladeHandle = glade_xml_new(file, root);
        glade_xml_signal_autoconnect_full();
    }

    /**
     * Create a new LibGlade object from an Reader which defines a Glade XML
     * definition. This definition is read, parsed, and a GTK widget set is
     * mapped. Each LibGlade object is "owned" by an object that provides event
     * handling for all the defined widgets.
     * 
     * This class supports the <a
     * href="http://glade.gnome.org/glade-2.0.dtd">Glade 2.0 XML schema</a>.
     * Glade 1.0 files (those that start with the
     * <code>&lt;GTK-Interface&gt;</code> tag) can be converted using
     * libglade-convert (part of the <code>libglade2-devel</code> package).
     * 
     * An optional widget node root may be specified to build a fragment of a
     * widget tree (specify <code>null</code>. From the LibGlade 2.0
     * documentation: "This feature is useful if you only want to build say a
     * toolbar or menu from the XML file, but not the window it is embedded in."
     * 
     * @param in
     *            the stream for a Glade 2.0 XML definition.
     * @param owner
     *            the default object for event handling
     * @param root
     *            the root widget node for a sub-tree, or <code>null</code>
     *            for the complete tree.
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */
    public LibGlade(InputStream in, Object owner, String root)
            throws GladeXMLException, IOException {
        this.owner = owner;
        byte[] buffer = scanXML(in);
        gladeHandle = glade_xml_new_from_buffer(buffer, root);
        glade_xml_signal_autoconnect_full();
    }

    /*
     * Lightly scan the XML stream for correctness, to improve on libglade's "it
     * works or it doesn't" error reporting.
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */
    private byte[] scanXML(InputStream in) throws GladeXMLException,
            IOException {
        CharArrayWriter caw = new CharArrayWriter(8192);
        PrintWriter pw = new PrintWriter(caw);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.indexOf("<GTK-Interface>") != -1)
                throw new GladeXMLException("obsolete Glade XML format");
            pw.println(line);
        }
        pw.close();
        return caw.toString().getBytes();
    }

    public Widget getWidget(String name) {
        Widget widget = (Widget) widgets.get(name);

        if (widget == null) {
            Handle handle = getNativeWidget(name);
            if (handle != null)
                widget = getWidget(handle);
            else
                System.err.println("getWidget: failed getting " + name);

        }

        return widget;
    }

    protected native Handle getNativeWidget(String name);

    protected native String getWidgetName(Handle nativepeer);

    protected void connect(String handler, Handle sourceHandle, String signal,
            String data, Handle targetHandle, boolean after)
            throws GladeXMLException {

        Widget source = getWidget(sourceHandle);
        if (source == null)
            throw new GladeXMLException("invalid source widget handle");
        Widget target = (targetHandle != null) ? getWidget(targetHandle) : null;

        Class listenerClass = source.getEventListenerClass(signal);
        if (listenerClass != null) {
            try {
                Object listener = ListenerDelegate.create(signal,
                        listenerClass, owner, handler, target);
                Method m = source.getClass().getMethod("addListener",
                        new Class[] { listenerClass });
                m.invoke(source, new Object[] { listener });
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        } else {
            // System.err.println( "can't find signal: " + signal );
        }
    }

    protected Widget getWidget(Handle handle) {
        String widgetName = getWidgetName(handle);
        if (widgetName == null) {
            return null;
        }

        Widget widget = (Widget) widgets.get(widgetName);
        if (widget == null) {
            try {
                widget = Widget.makeWidget(handle);
                widgets.put(widgetName, widget);
                widgets.put(getWidgetName(handle), widget);
            } catch (ClassNotFoundException e) {
                System.err
                        .println("could not create widget: " + e.getMessage());
            }
        }

        return widget;
    }

    /**
     * Create a new GladeXML native object from a Glade XML definition. The
     * gladeHandle instance variable is initialized with this method. Passes the
     * filename of the glade file directly to LibGlade rather than buffering it
     * as an InputStream. This avoids obscure bugs when instantiating GtkPixbufs
     * in the native upstream LibGlade code.
     * 
     * @param filename
     *            the glade file to pull the description from
     * @param root
     *            the root widget node for a sub-tree, or <code>null</code>
     *            for the complete tree.
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */
    private native Handle glade_xml_new(String filename, String rootname)
            throws GladeXMLException;

    /**
     * Create a new GladeXML native object from a Glade XML definition. The
     * gladeHandle instance variable is initialized with this method.
     * 
     * @param buffer
     *            an array containing the Glade XML definition.
     * @param root
     *            the root widget node for a sub-tree, or <code>null</code>
     *            for the complete tree.
     * @deprecated Superceeded by java-gnome 4.0; this method
     *             will no doubt exist conceptually, but it likely has a different
     *             name or signature due to the fact that the presented API is now
     *             an algorithmic mapping of the underlying native libraries.
     */

    private native Handle glade_xml_new_from_buffer(byte[] buffer,
            String rootname) throws GladeXMLException;

    private native void glade_xml_signal_autoconnect_full()
            throws GladeXMLException;

    private static native void initIDs();

    static {
        System.loadLibrary(Config.LIBRARY_NAME + Config.GLADE_API_VERSION);

        initIDs();
    }
}
