package org.gnu.glade;

import java.lang.reflect.Method;

import org.gnu.gtk.event.CellRendererTextEvent;
import org.gnu.gtk.event.CellRendererTextListener;

/**
 * CellRendererTextListener delegate class.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
class CellRendererTextDelegate extends ListenerDelegate implements
        CellRendererTextListener {

    public CellRendererTextDelegate(String signal, Object owner,
            Method handler, Object target) throws NoSuchMethodException {
        super(signal, owner, handler, target);
    }

    public void cellRendererTextEvent(CellRendererTextEvent event) {
        fireEvent(event);
    }
}
