/* 
 * LibGlade support for libglade for Java-Gnome
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glade;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.gnu.glib.EventType;
import org.gnu.gtk.Widget;
import org.gnu.gtk.event.GtkEvent;

/**
 * ListenerDelegate: abstract base class for Java-GNOME event listener delegate
 * classes which connect to glade-specified event handlers.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
abstract class ListenerDelegate {

    protected String signal; // the signal being fired by GTK

    protected Object owner; // the object which contains custom handler

    // methods
    protected Method handler; // the owner's handler method for this

    // signal
    protected Object target; // the optional object to send the handler

    public ListenerDelegate(String signal, Object owner, Method handler,
            Object target) throws NoSuchMethodException {

        this.signal = signal;
        this.owner = owner;
        this.handler = handler;
        this.target = target;
    }

    private static Method getHandlerMethod(Object owner, String handlerName)
            throws NoSuchMethodException {

        Method[] methods = owner.getClass().getMethods();
        int n = methods.length;
        for (int i = 0; i < n; i++) {
            Method m = methods[i];
            if (m.getName().equals(handlerName)) {
                return m;
            }
        }
        throw new NoSuchMethodException(owner.getClass().getName() + '.'
                + handlerName);
    }

    public boolean fireEvent(GtkEvent event) {
        return fireEvent(event, 0.0);
    }

    public boolean fireEvent(GtkEvent event, double value) {
        Object[] params = null;
        Object ret = null;
        try {
            Widget source = (Widget) event.getSource();
            // System.err.println("fireEvent: source:"+source);
            EventType typeNeeded = source.getEventType(signal);
            // System.err.println("fireEvent: typeNeeded:"+typeNeeded);
            if (event.getType() == typeNeeded) {
                Class[] paramTypes = handler.getParameterTypes();

                // handlers have optional event and target parameters
                int n = paramTypes.length;
                // System.err.println("fireEvent: n:"+n);
                if (n == 2)
                    // <handler>(event, target);
                    params = new Object[] { event, target };
                else if (n == 1)
                    // <handler>(event);
                    params = new Object[] { event };
                else
                    // <handler>();
                    params = new Object[0];

                // System.err.println("fireEvent: owner:"+owner+",
                // event:"+event+", target:"+target);
                ret = handler.invoke(owner, params);

                // return type is either null (if void) or boolean
                if (ret != null && ret.getClass().equals(Boolean.TYPE))
                    return ((Boolean) ret).booleanValue();
                else
                    return false;
            }
            /* else ignore event */
        } catch (InvocationTargetException e) {
            System.err.println("ListenerDelegate.fireEvent(" + event + "): "
                    + e.getTargetException());
            e.getTargetException().printStackTrace();
        } catch (Exception e) {
            System.err.println("ListenerDelegate.fireEvent(" + event + "): "
                    + e);
            e.printStackTrace();
            System.err.println("params:" + params + ", ret:" + ret);
        }
        return false;
    }

    static ListenerDelegate create(String signal, Class listenerClass,
            Object owner, String handlerName, Object target)
            throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException {

        try {
            Method handler = getHandlerMethod(owner, handlerName);
            Class delegateClass = (Class) delegateClassMap.get(listenerClass);
            if (delegateClass == null) {
                String listenerClassName = listenerClass.getName();
                String delegateClassName = (String) delegateMap
                        .get(listenerClassName);

                delegateClass = Class.forName(delegateClassName);
                delegateClassMap.put(listenerClass, delegateClass);
            }
            if (delegateClass == null)
                throw new ClassNotFoundException("No libglade delegate for "
                        + listenerClass.getName());

            Constructor ctor = delegateClass.getConstructor(new Class[] {
                    String.class, Object.class, Method.class, Object.class });
            return (ListenerDelegate) ctor.newInstance(new Object[] { signal,
                    owner, handler, target });
        } catch (NoSuchMethodException e) {
            throw e;
        } catch (InvocationTargetException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvocationTargetException(e, "ListenerDelegate - "
                    + "create failure!" + " signal=" + signal
                    + " listenerClass=" + listenerClass
                    + ". Please file a bug with this full " + "stack trace.");
        }
    }

    // Map GTK and GNOME listener interfaces to their respective delegates.
    private static Map delegateMap = new HashMap();

    private static Map delegateClassMap = new HashMap();
    static {
        delegateMap.put("org.gnu.gtk.event.AdjustmentListener",
                "org.gnu.glade.AdjustmentDelegate");
        delegateMap.put("org.gnu.gtk.event.ButtonListener",
                "org.gnu.glade.ButtonDelegate");
        delegateMap.put("org.gnu.gtk.event.CalendarListener",
                "org.gnu.glade.CalendarDelegate");
        delegateMap.put("org.gnu.gtk.event.CellEditableListener",
                "org.gnu.glade.CellEditableDelegate");
        delegateMap.put("org.gnu.gtk.event.CellRendererTextListener",
                "org.gnu.glade.CellRendererTextDelegate");
        delegateMap.put("org.gnu.gtk.event.CheckMenuItemListener",
                "org.gnu.glade.CheckMenuItemDelegate");
        delegateMap.put("org.gnu.gtk.event.ColorButtonListener",
                "org.gnu.glade.ColorButtonDelegate");
        delegateMap.put("org.gnu.gtk.event.ComboBoxListener",
                "org.gnu.glade.ComboBoxDelegate");
        delegateMap.put("org.gnu.gtk.event.ContainerListener",
                "org.gnu.glade.ContainerDelegate");
        delegateMap.put("org.gnu.gtk.event.DialogListener",
                "org.gnu.glade.DialogDelegate");
        delegateMap.put("org.gnu.gtk.event.EntryListener",
                "org.gnu.glade.EntryDelegate");
        delegateMap.put("org.gnu.gtk.event.ExpanderListener",
                "org.gnu.glade.ExpanderDelegate");
        delegateMap.put("org.gnu.gtk.event.ExposeListener",
                "org.gnu.glade.ExposeDelegate");
        delegateMap.put("org.gnu.gtk.event.FocusListener",
                "org.gnu.glade.FocusDelegate");
        delegateMap.put("org.gnu.gtk.event.FontButtonListener",
                "org.gnu.glade.FontButtonDelegate");
        delegateMap.put("org.gnu.gtk.event.ItemListener",
                "org.gnu.glade.ItemDelegate");
        delegateMap.put("org.gnu.gtk.event.KeyListener",
                "org.gnu.glade.KeyDelegate");
        delegateMap.put("org.gnu.gtk.event.LifeCycleListener",
                "org.gnu.glade.LifeCycleDelegate");
        delegateMap.put("org.gnu.gtk.event.MenuItemListener",
                "org.gnu.glade.MenuItemDelegate");
        delegateMap.put("org.gnu.gtk.event.MouseListener",
                "org.gnu.glade.MouseDelegate");
        delegateMap.put("org.gnu.gtk.event.MouseMotionListener",
                "org.gnu.glade.MouseMotionDelegate");
        delegateMap.put("org.gnu.gtk.event.NotebookListener",
                "org.gnu.glade.NotebookDelegate");
        delegateMap.put("org.gnu.gtk.event.OptionMenuListener",
                "org.gnu.glade.OptionMenuDelegate");
        delegateMap.put("org.gnu.gtk.event.RangeListener",
                "org.gnu.glade.RangeDelegate");
        delegateMap.put("org.gnu.gtk.event.ScaleListener",
                "org.gnu.glade.ScaleDelegate");
        delegateMap.put("org.gnu.gtk.event.SpinListener",
                "org.gnu.glade.SpinDelegate");
        delegateMap.put("org.gnu.gtk.event.TextBufferListener",
                "org.gnu.glade.TextBufferDelegate");
        delegateMap.put("org.gnu.gtk.event.TextViewListener",
                "org.gnu.glade.TextViewDelegate");
        delegateMap.put("org.gnu.gtk.event.ToggleListener",
                "org.gnu.glade.ToggleDelegate");
        delegateMap.put("org.gnu.gtk.event.ToolBarListener",
                "org.gnu.glade.ToolBarDelegate");
        delegateMap.put("org.gnu.gtk.event.ToolButtonListener",
                "org.gnu.glade.ToolButtonDelegate");
        delegateMap.put("org.gnu.gtk.event.TreeModelListener",
                "org.gnu.glade.TreeModelDelegate");
        delegateMap.put("org.gnu.gtk.event.TreeSelectionListener",
                "org.gnu.glade.TreeSelectionDelegate");
        delegateMap.put("org.gnu.gtk.event.TreeViewColumnListener",
                "org.gnu.glade.TreeViewColumnDelegate");
        delegateMap.put("org.gnu.gtk.event.TreeViewListener",
                "org.gnu.glade.TreeViewDelegate");

        delegateMap.put("org.gnu.gnome.AppBarListener",
                "org.gnu.glade.AppBarDelegate");
        delegateMap.put("org.gnu.gnome.ClientListener",
                "org.gnu.glade.ClientDelegate");
        delegateMap.put("org.gnu.gnome.ColorPickerListener",
                "org.gnu.glade.ColorPickerDelegate");
        delegateMap.put("org.gnu.gnome.DateEditListener",
                "org.gnu.glade.DateEditDelegate");
        delegateMap.put("org.gnu.gnome.DruidPageChangeListener",
                "org.gnu.glade.DruidPageDelegate");
        delegateMap.put("org.gnu.gnome.DruidPageSetupListener",
                "org.gnu.glade.DruidPageDelegate");
        delegateMap.put("org.gnu.gnome.FontPickerListener",
                "org.gnu.glade.FontPickerDelegate");
    }
}
