/* 
 * LibGlade support for libglade for Java-Gnome
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glade;

import java.lang.reflect.Method;

/**
 * Command wrapper for LibGladeStubs, to catch and cleanly report missing class
 * dependencies.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class probably does not exist in java-gnome 4.0,
 *             but have a look in <code>org.gnome.glade</code>.
 */
public class GenerateStubs {
    private static void error(String message) {
        System.err.println(message);
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length != 1)
            error("usage: java org.gnu.glade.GenerateStubs <filename.glade>");

        try {
            Class cmdClass = Class.forName("org.gnu.glade.LibGladeStubs");
            Method m = cmdClass.getMethod("execute",
                    new Class[] { String.class });
            m.invoke(null, new Object[] { args[0] });
            System.exit(0);
        } catch (NoClassDefFoundError e) {
            error("Class " + e.getMessage() + " not found\n"
                    + "Either use a 1.4 or later JDK, "
                    + "or include xerces.jar in your classpath.");
        } catch (Exception e) {
            error(e.toString());
        }
    }

}
